/*
    Volume-popup - simple popup\cloud for Box-like WM's.
    Copyright (C) 2018 Volk_Milit (aka Ja'Virr-Dar)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef POPUP_H
#define POPUP_H

#include <QWidget>

namespace Ui {class popup;}

class popup : public QWidget
{
    Q_OBJECT

    public:
        explicit popup(QWidget *parent = 0);
        ~popup();

        void updateSlider();

    signals:
        void sliderChanges();

    private slots:
        void on_btn_mixer_clicked();
        void on_hs_volumelevel_valueChanged(int value);
        void on_btn_stopstart_pulse_clicked();

    private:
        Ui::popup *ui;
        bool checkProcess(const QString &name);
};

#endif // POPUP_H
