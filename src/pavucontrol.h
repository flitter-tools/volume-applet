#ifndef PAVUCONTROL_H
#define PAVUCONTROL_H

#include <QProcess>

class Pavucontrol
{
    public:
        Pavucontrol();
        virtual ~Pavucontrol();
        void start();

    private:
        QProcess *proc;
};

#endif // PAVUCONTROL_H
