#include "pavucontrol.h"

#include <QFile>
#include <QMessageBox>

Pavucontrol::Pavucontrol()
{
    proc = new QProcess();
}

Pavucontrol::~Pavucontrol()
{
    delete proc;
}

void Pavucontrol::start()
{
    QString binary = "/usr/bin/pavucontrol";
    QFile pavucontrol(binary);
    if (!pavucontrol.exists())
    {
        binary = "/usr/bin/pavucontrol-qt";
        QFile pavucontrol(binary);

        if (!pavucontrol.exists())
        {
            QMessageBox::warning(nullptr, "Warning!", "You seems doesn't have install pavucontrol, please install it.", QMessageBox::Ok);
            return;
        }
    }

    proc->startDetached(binary);
}
