/*
    Volume-popup - simple popup\cloud for Box-like WM's.
    Copyright (C) 2018 Volk_Milit (aka Ja'Virr-Dar)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QIcon>
#include <QCoreApplication>
#include <QPixmap>
#include <QImage>
#include <QColor>
#include <QWheelEvent>
#include <QPainter>

#include "alsa.h"
#include "tray.h"
#include "pavucontrol.h"

tray::tray(QSystemTrayIcon *parent) : QSystemTrayIcon(parent)
{
    a_quit = new QAction("Quit", this);
    connect(a_quit, &QAction::triggered, qApp, &QCoreApplication::quit);

    tmenu = new QMenu;
    tmenu->addAction(a_quit);

    ipopup = new popup();

    connect(ipopup, &popup::sliderChanges, this, &tray::updateIcon);

    updateIcon();
    setContextMenu(tmenu);

    connect(this, &QSystemTrayIcon::activated, this, &tray::tpopup);
}

tray::~tray()
{
    delete ipopup;
    delete a_quit;
    delete tmenu;
}

void tray::tpopup(QSystemTrayIcon::ActivationReason reason)
{
    if (reason == QSystemTrayIcon::Trigger)
    {
        ipopup->updateSlider();
        ipopup->show();
    }

    if (reason == QSystemTrayIcon::MiddleClick)
    {
        Pavucontrol contr;
        contr.start();
    }
}

bool tray::event(QEvent *event)
{
    if (event->type() == QEvent::Wheel)
    {
        long vol = Alsa::getVolume();

        QWheelEvent *wheelEvent = static_cast<QWheelEvent*>(event);

        if (vol <= 100 && vol >= 0)
        {
            if(wheelEvent->delta() > 0)
                Alsa::SetAlsaMasterVolume(vol + 5);
            else
                Alsa::SetAlsaMasterVolume(vol - 5);

            updateIcon();
        }
    }

    return true;
}

void tray::updateIcon()
{
    long vol = Alsa::getVolume();

    QIcon icon;

    if (vol >= 80)
        icon = QIcon::fromTheme("audio-volume-high");
    else if (vol >= 30)
        icon = QIcon::fromTheme("audio-volume-medium");
    else if (vol >= 10)
        icon = QIcon::fromTheme("audio-volume-low");
    else
        icon = QIcon::fromTheme("audio-volume-low");


    QPixmap pix = icon.pixmap(icon.availableSizes().first());
    QPainter painter(&pix);
    painter.setCompositionMode(QPainter::CompositionMode_SourceIn);
    painter.fillRect(pix.rect(), "#CDD6DA");

    if (vol <= 0)
    {
        QImage image(":/images/sound-off.svg");
        painter.setCompositionMode(QPainter::CompositionMode_SourceOver);
        painter.drawImage(0, 0, image);
    }

    painter.end();
    pix = pix.scaled(14, 14, Qt::KeepAspectRatio, Qt::SmoothTransformation);
    this->setIcon(QIcon(pix));
}
