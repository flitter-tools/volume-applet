/*
    Volume-popup - simple popup\cloud for Box-like WM's.
    Copyright (C) 2018 Volk_Milit (aka Ja'Virr-Dar)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "popup.h"
#include "ui_popup.h"

#include "alsa.h"
#include "pavucontrol.h"
#include <QProcess>
#include <QDesktopWidget>
#include <QApplication>
#include <QDirIterator>

popup::popup(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::popup)
{
    ui->setupUi(this);

    this->setWindowFlags(Qt::Popup | Qt::FramelessWindowHint);

    this->move(QApplication::desktop()->width() - this->width(),\
                   QApplication::desktop()->height() - this->height() - 25);

    QIcon icon = QIcon::fromTheme("gnome-volume-control");
    QPixmap pix = icon.pixmap(icon.availableSizes().first());
    ui->l_icon->setPixmap(pix);

    int vol = Alsa::getVolume();

    if (vol)
    {
        ui->hs_volumelevel->setValue(vol);
        ui->l_status->setHidden(true);
    }
    else
    {
        ui->l_status->setText("Failed to get volume from Default device.");
        ui->l_status->setHidden(false);
    }
}

popup::~popup()
{
    delete ui;
}

void popup::updateSlider()
{
    ui->hs_volumelevel->setValue(Alsa::getVolume());

    if (!checkProcess("pulseaudio"))
    {
        ui->l_status->setHidden(false);
        ui->l_status->setText("Failed to connect to pulseaudio. Is daemon running?");
        ui->btn_stopstart_pulse->setIcon(QIcon::fromTheme("media-playback-start"));
    }
    else
    {
        ui->l_status->setHidden(true);
        ui->btn_stopstart_pulse->setIcon(QIcon::fromTheme("stop"));
    }
}

void popup::on_btn_mixer_clicked()
{
    Pavucontrol contr;
    contr.start();
    this->hide();
}

void popup::on_hs_volumelevel_valueChanged(int value)
{
    Alsa::SetAlsaMasterVolume(static_cast<long>(value));
    ui->l_volumelevel->setText(QString::number(value));
    emit sliderChanges();
}

bool popup::checkProcess(const QString &name)
{
    bool found = false;
    QDirIterator it("/proc");

    QString line;

    while (it.hasNext())
    {
        QFile file(it.next() + "/cmdline");

        if (!file.exists())
            continue;

        file.open(QIODevice::ReadOnly);
        QTextStream in(&file);

        line = in.readLine();

        if (line.contains(name))
        {
            file.close();
            found = true;
            break;
        }

        file.close();
    }

    return found;
}

void popup::on_btn_stopstart_pulse_clicked()
{
    QProcess *proc = new QProcess(this);

    if (checkProcess("pulseaudio"))
    {
        proc->start("pulseaudio -k");
        proc->waitForFinished();
    }
    else
    {
        proc->start("pulseaudio --start");
        proc->waitForFinished();
    }

    proc->deleteLater();
}
