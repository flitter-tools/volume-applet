#ifndef ALSA_H
#define ALSA_H

#include <alsa/asoundlib.h>
#include <alsa/mixer.h>

#include <QDebug>

class Alsa
{
public:

    static int getVolume()
    {
        int error;
        long min;
        long max;
        long level;
        snd_mixer_t* handle;
        snd_mixer_selem_id_t* sid;
        snd_mixer_elem_t* elem;
        error = snd_mixer_open(&handle, 0);

        char* card = strdup("default");
        char* channel = strdup("Master");

        if(error < 0)
            return -1;

        error = snd_mixer_attach(handle, card);

        if(error < 0)
        {
            snd_mixer_close(handle);
            return -1;
        }

        error = snd_mixer_selem_register(handle, NULL, NULL);

        if(error < 0)
        {
            snd_mixer_close(handle);
            return -1;
        }

        error = snd_mixer_load(handle);

        if(error < 0)
        {
            snd_mixer_close(handle);
            return -1;
        }

        snd_mixer_selem_id_alloca(&sid);
        snd_mixer_selem_id_set_index(sid, 0);
        snd_mixer_selem_id_set_name(sid, channel);
        elem = snd_mixer_find_selem(handle, sid);

        if(elem == NULL)
        {
            snd_mixer_close(handle);
            return -1;
        }

        error = snd_mixer_selem_get_playback_volume_range(elem, &min, &max);

        if(error < 0)
        {
            snd_mixer_close(handle);
            return -1;
        }

        error = snd_mixer_selem_get_playback_volume(elem, SND_MIXER_SCHN_MONO,
                &level);

        if(error < 0)
        {
            snd_mixer_close(handle);
            return -1;
        }

        return static_cast<int>((100 * (level - min) / max));
    }

    static void SetAlsaMasterVolume(long volume)
    {
        long min, max;
        snd_mixer_t *handle;
        snd_mixer_selem_id_t *sid;
        const char *card = "default";
        const char *selem_name = "Master";

        snd_mixer_open(&handle, 0);
        snd_mixer_attach(handle, card);
        snd_mixer_selem_register(handle, NULL, NULL);
        snd_mixer_load(handle);

        snd_mixer_selem_id_alloca(&sid);
        snd_mixer_selem_id_set_index(sid, 0);
        snd_mixer_selem_id_set_name(sid, selem_name);
        snd_mixer_elem_t* elem = snd_mixer_find_selem(handle, sid);

        snd_mixer_selem_get_playback_volume_range(elem, &min, &max);
        snd_mixer_selem_set_playback_volume_all(elem, volume * max / 100);

        snd_mixer_close(handle);
    }

    static void SetAlsaMasterMute()
    {
        int ival;
        snd_mixer_t *handle;
        snd_mixer_selem_id_t *sid;
        const char *card = "pulse";
        const char *selem_name = "Master";

        snd_mixer_open(&handle, 0);
        snd_mixer_attach(handle, card);
        snd_mixer_selem_register(handle, NULL, NULL);
        snd_mixer_load(handle);

        snd_mixer_selem_id_alloca(&sid);
        snd_mixer_selem_id_set_index(sid, 0);
        snd_mixer_selem_id_set_name(sid, selem_name);
        snd_mixer_elem_t* elem = snd_mixer_find_selem(handle, sid);

        if (snd_mixer_selem_has_playback_switch(elem))
        {
            snd_mixer_selem_get_playback_switch(elem, static_cast<snd_mixer_selem_channel_id_t>(0), &ival);
            snd_mixer_selem_set_playback_switch(elem, static_cast<snd_mixer_selem_channel_id_t>(0), (ival ? 1 : 0) ^ 1);
        }

        snd_mixer_close(handle);
    }
};

#endif // ALSA_H
